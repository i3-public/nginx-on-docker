
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/swap/README.txt | bash -s 4G

size=$1
file="/swapfile_"$size

fallocate -l $size $file

chmod 600 $file
mkswap $file
swapon $file
cp /etc/fstab /etc/fstab.bk
echo '' >> /etc/fstab
echo $file' none swap sw 0 0' | sudo tee -a /etc/fstab
free -m

