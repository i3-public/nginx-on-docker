
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/certbot/README.txt | bash

conf=https://gitlab.com/i3-public/conf/-/raw/main/on-docker/raw-ubuntu/README.txt
patch=https://gitlab.com/i3-public/conf/-/raw/main/certbot/conf/patch

wget -qO- $conf | bash -s certbot '-v /root/ssl:/ssl -p 80:80 -p 443:443 --hostname '$(hostname) $patch

