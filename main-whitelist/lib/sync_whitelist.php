<?php


$file = '/home/xtreamcodes/iptv_xtream_codes/main-whitelist/.env';
if(! file_exists($file) ){
    die('no .env found.');
} else {
    $file = file($file);
    if( sizeof($file) ){
        foreach( $file as $line ){
            if(! $line = trim($line,"\r\t\n ") )
                continue; 
            if( substr($line, 0, 1) == '#' )
                continue;
            if(! strstr($line, '=') )
                continue;
            $pos = strpos($line, '=');
            $k = substr($line, 0, $pos);
            $v = substr($line, $pos+1 );
            if( strtolower($v) === 'false' ){
                $v = false;
            } else if( strtolower($v) === 'true' ){
                $v = true;
            } else if( strtolower($v) === 'null' ) {
                $v = null;
            }
            define($k, $v);
        }
    }
}



if(! $json = fgct( SIGNAL_POINT.'/api/feed/etc/server_list/' ) ){
	echo "can't connect to signal point\n";

} else {

	$ip_s = json_decode($json, true);

	if(! sizeof($ip_s) ){
		echo "no ip found\n";

	} else {
	    access_for_nginx($ip_s);
	}

}



function access_for_nginx( $ip_s ){
	
	$file = '/home/xtreamcodes/iptv_xtream_codes/nginx/conf/whitelist.conf';
	$server_ip = trim( shell_exec(" hostname -I | awk {'print \$1'}"), "\r\n\t ");

	$bl = "allow 127.0.0.1;\n";
	$bl.= "allow ".$server_ip.";\n";

	foreach( $ip_s as $section => $list ){
		foreach( $list as $ip_n_port => $name ){
			list($ip, $port) = explode(':', $ip_n_port);
			$bl.= "allow $ip;\n";
		}
	}

	$bl.= "deny all;\n";

	if( !file_exists($file) or file_get_contents($file) != $bl ){
		file_put_contents($file, $bl);
		echo shell_exec("/home/xtreamcodes/iptv_xtream_codes/nginx/sbin/nginx -s reload");
		echo "done\n";
	
	} else {
		echo "same\n";
	}

}



# 1 Aug 2020
function fgct( $url, $timeout=4, $error=false ){
	
	$contx = stream_context_create([

		'http' => [
			'timeout' => intval($timeout),
		],

	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],

    ]);

	if( $error ){
		return file_get_contents( $url, false, $contx );
	
	} else {
		return @file_get_contents( $url, false, $contx );
	}
	
}


