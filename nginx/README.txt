
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/nginx/README.txt | bash -s 80

port=$1

apt -y install nginx

rm -rf /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available /etc/nginx/sites-enabled

wget https://gitlab.com/i3-public/conf/-/raw/main/nginx/conf/nginx.conf -O /etc/nginx/nginx.conf
wget https://gitlab.com/i3-public/conf/-/raw/main/nginx/conf/default -O /etc/nginx/sites-available/default

sed -i 's/listen 80;/listen '$port';/g' /etc/nginx/sites-available/default

rm -rf /var/www/html/index.nginx-debian.html
service nginx reload

echo "/usr/sbin/nginx -g 'daemon off;' &" >> /etc/rc.local
