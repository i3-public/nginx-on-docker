#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/mysql-on-docker/README.txt | bash -s pass

# docker network create --subnet=172.23.0.0/16 moss
# docker run -d --name mariadb -p 3306:3306 -e MARIADB_ROOT_PASSWORD=$pass --net moss --ip 172.23.0.33 mariadb/server:10.3
# docker run -d --name pma --net moss -e PMA_HOST=172.23.0.33 -p 6060:80 phpmyadmin

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash

any_moss_netw=$(docker network ls | grep moss | awk {'print $2'})

if [ "$any_moss_netw" != "moss" ]; then
    docker network create --subnet=172.23.0.0/16 moss
fi

ip=$(hostname -I | awk {'print $1'})

if [ "$1" == "" ]; then
  pass=$(md5sum /etc/passwd | awk {'print $1'})
else
  pass=$1
fi

docker run -dt --name mariadb -p 3306:3306 -e MARIADB_ROOT_PASSWORD=$pass --net moss --ip 172.23.0.33 mariadb:10.3
docker run -dt --name pma --net moss -e PMA_HOST=172.23.0.33 -p 6060:80 phpmyadmin

echo $pass > mysql-root-password
docker exec mariadb bash -c " sed -i 's/max_connections.*/sql-mode=\"NO_ENGINE_SUBSTITUTION,NO_AUTO_CREATE_USER\"\nmax_connections = 2000/g' /etc/mysql/my.cnf "

docker restart mariadb

echo ""
echo "pma: http://"$ip":6060"
echo "user: root"
echo "pass: "$pass

echo ""

