# 
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/vncz/README.txt | bash

# after installing ovz7 https://download.openvz.org/virtuozzo/releases/openvz-7.0.19-347/x86_64/iso/openvz-iso-7.0.19.iso
# to have a new node: vzctl create 1 --ostemplate ubuntu-20.04 --config VZ2GB --ipadd 148.251.24.0 --hostname nil.i3vnc.com

wget -qO- https://gitlab.com/i3-public/public-text/-/raw/master/preinstall-raw-os | bash
wget -O /etc/vz/conf/ve-VZ2GB.conf-sample https://gitlab.com/i3-public/conf/-/raw/main/vncz/conf/ve-VZ2GB.conf-sample

wget -O /usr/bin/addvnc https://gitlab.com/i3-public/conf/-/raw/main/vncz/conf/addvnc
chmod +x /usr/bin/addvnc

vzctl create 1 --ostemplate ubuntu-20.04 --config VZ2GB --ipadd 192.168.1.100 --hostname nil
vzctl delete 1

wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s 80 skipserver

echo "";
echo "to add a new vnc, run: addvnc kemen 148.251.24.2";
echo "";


