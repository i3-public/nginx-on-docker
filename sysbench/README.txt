
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/sysbench/README.txt | bash

if [ ! -f /usr/bin/sysbench ]; then
    apt -y install sysbench >/dev/null 2>&1
fi

n=$(cat /proc/cpuinfo | grep processor  | wc -l)
# echo "sysbench --threads="$n" cpu run"

sysbench --threads=$n cpu run | grep 'total number of events' | cut -d: -f2 | xargs

