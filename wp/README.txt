
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/wp/README.txt | bash

apt install unzip -y
wget https://wordpress.org/latest.zip
unzip -q latest.zip
rm -rf latest.zip
cd wordpress
mv * ..
cd ..
rmdir wordpress
cd ..
chown -R www-data:www-data html
