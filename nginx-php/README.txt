#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/README.txt | bash -s ver name port patch_url q_etc

phpv=$1
name=$2
port=$3
patch=$4
q_etc=$5

image=$name"-image"
sshp=$(($port-1))

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash

wget -O /tmp/dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/conf/dockerfile
sed -i 's/7.4/'$phpv'/g' /tmp/dockerfile

if [ "$patch" != "" ] && [ "$patch" != "--" ]; then
  sed -i 's,#-PATCH-HERE-#,RUN wget -qO- --no-check-certificate --no-cache --no-cookies "'$patch'" | bash,g' /tmp/dockerfile
fi

# echo "removing docker: "$name" and docker-image: "$image
docker rm -f $name
docker rmi $image
docker system prune -af

docker build -t $image -f /tmp/dockerfile .
rm -rf /tmp/dockerfile

if [ "$q_etc" == "" ] || [ "$q_etc" == "--" ]; then
  q_etc=""
fi

echo "docker run -t -d --restart unless-stopped --name "$name" -p "$port":80 -p "$sshp":22 "$q_etc" "$image
docker run -t -d --restart unless-stopped --name $name -p $port:80 -p $sshp:22 $q_etc $image

