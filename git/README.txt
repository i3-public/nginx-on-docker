
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/git/README.txt | bash

apt -y update
apt -y install git

git config --global user.email "jefp23@protonmail.com"
git config --global user.name "Jeff Peterson"

git config --global --add safe.directory /var/www
