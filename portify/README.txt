
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/portify/README.txt | bash -s server_ip ssh_port ssh_password tunnel_in tunnel_out

apt -y update
apt -y install ssh sshpass

if [ ! -f ~/this_portify ]; then

  server_ip=$1
  ssh_port=$2
  ssh_password=$3
  tunnel_in=$4
  tunnel_out=$5

  wget https://gitlab.com/i3-public/conf/-/raw/main/portify/conf/portify -O /usr/bin/portify 2>/dev/null
  chmod +x /usr/bin/portify
  echo "bash /usr/bin/portify "$server_ip" "$ssh_port" "$ssh_password" "$tunnel_in" "$tunnel_out" &" > ~/this_portify
  echo "bash ~/this_portify" >> /etc/rc.local
  
fi

bash ~/this_portify


