
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/ffmpeg/README.txt | bash
# 

if [ `apt list --installed 2>/dev/null | grep ffmpeg | wc -l` == "0" ]; then
  apt -y install ffmpeg
fi