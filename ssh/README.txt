
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/ssh/README.txt | bash <-s 22>
#


if [ ! -f /etc/ssh/sshd_config ]; then

    apt -y update
    apt -y install ssh

    sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

    if [ "$1" != "" ]; then
        sshp=$1
        sed -i "s/#Port 22/Port "$sshp"/g" /etc/ssh/sshd_config
    fi

    service ssh restart
    echo "service ssh start" >> /etc/rc.local

fi

