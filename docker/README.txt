# 
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash


installed=$(apt list --installed 2>/dev/null | grep docker-ce-cli | wc -l)

if [ "$installed" == "0" ]; then

    echo "installing docker .."

    # upgrade the curl
    apt-mark unhold libcurl3
    apt -y remove libcurl3
    apt -y install ca-certificates curl

    # remove old docker
    dpkg -l | grep -i docker | awk {'print $2'} | xargs apt -y remove
    apt -y remove docker docker-engine docker.io containerd runc

    # install repo
    apt -y update
    sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt -y update

    # install docker
    sudo apt -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
 
    # hold this version
    # apt-mark hold docker-ce docker-ce-cli

    # keyrings
    install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
    chmod a+r /etc/apt/keyrings/docker.asc

    # start
    apt -y remove golang-docker-credential-helpers
    sudo systemctl start docker
    sudo systemctl enable docker

    # docker network
    any_moss_netw=$(docker network ls | grep moss | awk {'print $2'})
    if [ "$any_moss_netw" != "moss" ]; then
        docker network create --subnet=172.23.0.0/16 moss
    fi

    # shortcuts
    cd /usr/bin; rm -rf dk dkl dku; cd
    wget https://gitlab.com/i3-public/conf/-/raw/main/docker/conf/dk  -O /usr/bin/dk; chmod +x /usr/bin/dk
    wget https://gitlab.com/i3-public/conf/-/raw/main/docker/conf/dkl -O /usr/bin/dkl; chmod +x /usr/bin/dkl
    wget https://gitlab.com/i3-public/conf/-/raw/main/docker/conf/dku -O /usr/bin/dku; chmod +x /usr/bin/dku

else
    echo "docker already installed."
fi


