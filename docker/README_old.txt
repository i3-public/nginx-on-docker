# 
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash


installed=$(apt list --installed 2>/dev/null | grep docker-ce-cli | wc -l)

if [ "$installed" == "0" ]; then

    echo "installing docker .."

    apt -y remove docker docker-engine docker.io containerd runc
    
    dpkg -l | grep -i docker | awk {'print $2'} | xargs apt -y remove
    cd /usr/bin; rm -rf dk dkl dku; cd

    apt -y update
    apt -y install apt-transport-https ca-certificates gnupg-agent software-properties-common # curl
    wget -qO- https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt -y update
    apt -y install docker-ce docker-ce-cli containerd.io docker-compose

    apt -y remove golang-docker-credential-helpers

    systemctl start docker

    # moss network
    any_moss_netw=$(docker network ls | grep moss | awk {'print $2'})
    if [ "$any_moss_netw" != "moss" ]; then
        docker network create --subnet=172.23.0.0/16 moss
    fi

    # shortcuts
    wget https://gitlab.com/i3-public/conf/-/raw/main/docker/conf/dk  -O /usr/bin/dk; chmod +x /usr/bin/dk
    wget https://gitlab.com/i3-public/conf/-/raw/main/docker/conf/dkl -O /usr/bin/dkl; chmod +x /usr/bin/dkl
    wget https://gitlab.com/i3-public/conf/-/raw/main/docker/conf/dku -O /usr/bin/dku; chmod +x /usr/bin/dku

else
    echo "docker already installed."
fi


