wget -O dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile
docker build -t nginx-image -f dockerfile .
docker run -t -d --restart unless-stopped --name nginx-host -p 2022:22 -p 2080:80 -p 20443:443 nginx-image
