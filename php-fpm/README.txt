
# 
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/php-fpm/README.txt | bash -s 8.3

v=$1

apt -y install software-properties-common apt-transport-https
add-apt-repository ppa:ondrej/php -y
apt -y update
apt -y upgrade

apt -y install php$v-fpm php$v-curl php$v-zip php$v-gd php$v-mysql php$v-xml php$v-mbstring php$v-intl
service php$v-fpm start

ln -s /var/run/php/php$v-fpm.sock /var/run/php.sock
# ln -s /usr/sbin/php-fpm$v /usr/sbin/php-fpm

echo "listen = /var/run/php.sock" >> /etc/php/$v/fpm/php-fpm.conf
echo "listen.owner = www-data"    >> /etc/php/$v/fpm/php-fpm.conf
echo "listen.group = www-data"    >> /etc/php/$v/fpm/php-fpm.conf
echo "listen.mode = 0660"         >> /etc/php/$v/fpm/php-fpm.conf

wget https://gitlab.com/i3-public/conf/-/raw/main/php-fpm/conf/www.conf  -O /etc/php/$v/fpm/pool.d/www.conf
wget https://gitlab.com/i3-public/conf/-/raw/main/php-fpm/conf/php$v.ini -O /etc/php/$v/fpm/php.ini

rm -rf /etc/php/$v/cli/php.ini
ln -s /etc/php/$v/fpm/php.ini /etc/php/$v/cli/php.ini

if [ ! -d /run/php ]; then mkdir /run/php; fi

/etc/init.d/php$v-fpm restart
echo "/usr/sbin/php-fpm"$v >> /etc/rc.local
