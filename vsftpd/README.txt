# 
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/vsftpd/README.txt | bash -s USER HOME

apt -y install vsftpd
wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/vsftpd/conf/vsftpd.conf > /etc/vsftpd.conf

echo "service vsftpd restart" >> /etc/rc.local

the_ip=$(wget -qO- http://tools.i3ns.net/get/myip)

USER=$1
# PASS=$( md5sum /etc/passwd  | awk {'print $1'} )
PASS=$( echo $the_ip""$USER | md5sum | awk {'print $1'} )

if [ "$2" == "" ]; then
  USER_HOME="/home/"$1
else
  USER_HOME=$2
fi

useradd -d $USER_HOME -p "$(openssl passwd -6 $PASS)" $USER

if [ ! -d "$USER_HOME" ]; then
  mkdir -p $USER_HOME
fi

chown -R $USER:$USER $USER_HOME

echo "pasv_address="$the_ip >> /etc/vsftpd.conf

echo '--'
echo 'USERNAME '$USER
echo 'PASSWORD '$PASS
echo '--'

