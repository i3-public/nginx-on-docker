
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/on-docker/s3/README.txt | bash -s 2052

mkdir -p /docker/s3
echo 's3.' > /docker/s3/index.htm

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/on-docker/nginx/README.txt | bash -s s3 $1 https://gitlab.com/i3-public/conf/-/raw/main/on-docker/s3/conf/patch '--hostname s3.i3ns.net -v /docker/s3:/var/www/html -p 60000-60009:60000-60009 -p 20:20 -p 21:21'

docker exec s3 bash -c 'chown -R s3:s3 /var/www/html'
docker exec s3 bash -c 'wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s '$1' skipserver'

