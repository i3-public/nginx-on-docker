
# 
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/on-docker/nginx/README.txt | bash -s name port patch etc

name=$1
port=$2

sshp=$(($port-1))
sslp=$(($port+1))
image=$name"-image"

docker rm -f $name
docker rmi $image

wget -O /tmp/dockerfile https://gitlab.com/i3-public/conf/-/raw/main/on-docker/nginx/conf/dockerfile 2>/dev/null

if [ "$3" != "" ] && [ "$3" != "--" ]; then
  sed -i 's,#-PATCH-HERE-#,RUN wget -qO- --no-check-certificate --no-cache --no-cookies "'$3'" | bash,g' /tmp/dockerfile
fi

docker build -t $image -f /tmp/dockerfile .
rm -rf /tmp/dockerfile

if [ "$4" == "" ] || [ "$4" == "--" ]; then
  etc=""
else
  etc=$4
fi

echo "docker run -t -d --restart unless-stopped --name "$name" -p "$port":80 -p "$sslp":443 -p "$sshp":22 "$etc" "$image
docker run -t -d --restart unless-stopped --name $name -p $port:80 -p $sslp:443 -p $sshp:22 $etc $image


