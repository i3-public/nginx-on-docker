
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/on-docker/php/README.txt | bash -s ver name patch_url q_etc

phpv=$1
name=$2
patch=$3
q_etc=$4

image=$name"-image"
sshp="22"$( echo $phpv | sed -r 's/\.//g' )

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash
wget -O /tmp/dockerfile https://gitlab.com/i3-public/conf/-/raw/main/on-docker/php/conf/dockerfile
sed -i 's/7.4/'$phpv'/g' /tmp/dockerfile

if [ "$patch" != "" ] && [ "$patch" != "-" ]; then
  sed -i 's,#-PATCH-HERE-#,RUN wget -qO- --no-check-certificate --no-cache --no-cookies "'$patch'" | bash,g' /tmp/dockerfile
fi

docker rm -f $name
docker system prune -af

docker build -t $image -f /tmp/dockerfile .
# rm -rf /tmp/dockerfile

if [ "$q_etc" == "" ] || [ "$q_etc" == "--" ]; then
  q_etc=""
fi

echo "docker run -t -d --restart unless-stopped --name "$name" -p "$sshp":22 "$q_etc" "$image
docker run -t -d --restart unless-stopped --name $name -p $sshp:22 $q_etc $image

