
#
# wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/on-docker/pr4tunnel/README.txt | bash -s server_ip ssh_password

server_ip=$1
ssh_password=$2

wget -O dockerpatch https://gitlab.com/i3-public/conf/-/raw/main/on-docker/pr4tunnel/conf/patch
sed -i "s/SERVER_IP/"$server_ip"/g" dockerpatch
sed -i "s/SSH_PASSWORD/"$ssh_password"/g" dockerpatch

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/on-docker/raw-ubuntu/README.txt | bash -s pr4t '--hostname pr4t -p 8080:8080 --cap-add=SYS_ADMIN --privileged'

docker exec pr4t bash -c 'sysctl -w net.ipv4.tcp_tw_reuse=1'

rm -rf dockerpatch

