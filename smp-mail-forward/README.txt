
# docker https://hub.docker.com/r/zixia/simple-mail-forwarder/

export SMF_CONFIG='@mailcaps.com:mailcaps.com@gmail.com;@iptvtree.com:mailcaps.com@gmail.com;@i3ns.net:mailcaps.com@gmail.com;@xwork.app:mailcaps.com@gmail.com'
docker run -t -d --restart=always -e SMF_CONFIG -p 25:25 --name mailfwd zixia/simple-mail-forwarder


# mx record
mail             300     IN    A   88.198.67.242
@                300     IN    MX  0 mail

